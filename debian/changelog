libmysql-diff-perl (0.60-2) UNRELEASED; urgency=medium

  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2020 16:42:27 +0100

libmysql-diff-perl (0.60-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Remove TANIGUCHI Takaki from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.60.
  * Declare compliance with Debian Policy 4.1.5.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Fri, 20 Jul 2018 20:29:16 +0200

libmysql-diff-perl (0.50-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/rules: use 'wildcard' instead of 'shell echo'.
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Nick Morrott ]
  * Import upstream version 0.50
  * Declare compliance with Debian Policy 3.9.8
  * Bump debhelper compatibility level to 9
  * Refresh (build) dependencies
  * Update copyright years
  * Disable more tests that require a MySQL server
  * Remove d/t/p/smoke-skip (skipped tests moved to xt/)
  * Add debian/libmysql-diff-perl.docs
  * Refresh rename-script.patch
  * Update lintian overrides (library/application)
  * Add debian/upstream/metadata

 -- Nick Morrott <knowledgejunkie@gmail.com>  Fri, 12 Aug 2016 01:51:36 +0100

libmysql-diff-perl (0.43-3) unstable; urgency=low

  * Team upload

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * Mark package as autopkgtestable
  * Declare compliance with Debian Policy 3.9.6
  * Add explicit build dependency on libmodule-build-perl
  * skip meta/pod tests during autopkgtest

 -- Damyan Ivanov <dmn@debian.org>  Tue, 09 Jun 2015 14:42:46 +0000

libmysql-diff-perl (0.43-2) unstable; urgency=low

  * Rename mysqldiff script to mysql-schema-diff. (Closes: #663182)
  * Mention script name in long description. (Closes: #664120)
  * debian/copyright: update to Copyright-Format 1.0.
  * Bump years of packaging copyright.
  * Bump Standards-Version to 3.9.3 (no changes).
  * Add a lintian override (false positive about MySQL spelling).

 -- gregor herrmann <gregoa@debian.org>  Fri, 06 Apr 2012 17:14:21 +0200

libmysql-diff-perl (0.43-1) unstable; urgency=low

  * New upstream release.
  * Switch to source format 3.0 (quilt).
  * Stop installing README.
  * Update upstream copyright information.
  * Bump debhelper compatibility level to 8.
  * Add libfile-slurp-perl to Depends.
  * Run tests that don't need a MySQL server.
  * Add /me to Uploaders.
  * Update short and long description.

 -- gregor herrmann <gregoa@debian.org>  Fri, 07 Oct 2011 14:58:13 +0200

libmysql-diff-perl (0.41-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ TANIGUCHI Takaki ]
  * Imported Upstream version 0.41
  * Bump Standards-Version to 3.9.2.

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 06 Oct 2011 21:21:26 +0900

libmysql-diff-perl (0.33-1) unstable; urgency=low

  * Initial Release. (Closes: #611423)

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 04 Feb 2011 13:50:47 +0900
